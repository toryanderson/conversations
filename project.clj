(defproject conversations "0.1.0-SNAPSHOT"
  :description "Conversation scraping & analysis"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :jvm-opts ["-Xmx10g" "-Xms1024m" "-XX:MaxPermSize=2g"] 
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/java.jdbc "0.7.5"]
                 [cheshire "5.8.0"]
                 [migratus "1.0.6"]
                 [compojure "1.6.0"]
                 [org.postgresql/postgresql "42.1.4"]
                 [com.mchange/c3p0 "0.9.5.2"]
                 [ring/ring-defaults "0.1.5"]
                 [cheshire "5.8.0"]
                 [clj-http "3.7.0"]
                 [cprop "0.1.11"]
                 [enlive "1.1.6"]
                 [clj-tagsoup "0.3.0" :exclusions [org.clojure/clojure]]
                 [hikari-cp "1.7.6"]
                 [com.mchange/c3p0 "0.9.5.2"]
                 [honeysql "0.9.1"]
                 [mount "0.1.11"]
                 ;[hswick/jutsu.nlp "0.1.0"]
                 [org.nd4j/nd4j-native-platform "1.0.0-alpha"]
                 [org.deeplearning4j/deeplearning4j-nlp "1.0.0-alpha"]
                 [org.datavec/datavec-api "1.0.0-alpha"]
                 [ubergraph "0.5.0"]]
  :plugins [[lein-ring "0.9.7"]
            [lein-immutant "2.1.0"]]
  :resource-paths ["sql" "resources/config"]
  :migratus {:store :database
             :migrations-dir "sql"}
  :immutant {
             :war {
                   :name "conversations-%v%t"
                   :resource-paths ["war-resources"]
                   :context-path "/"}}
  :ring {:handler conversations.handler/app
         :uberwar-name "conversations.war"}
  
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
