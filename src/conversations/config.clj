(ns conversations.config
  (:require [cprop.core :as cpc]
            [cprop.source :as source]
            [mount.core :as mc]))

(mc/defstate env :start (cpc/load-config :merge
                                         [(mc/args)
                                          (source/from-env)]))
