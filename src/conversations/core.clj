(ns conversations.core
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [conversations.db.core :as db]
            [conversations.config :refer [env]])
  (:import [java.net URL]
           [java.io File]
           [org.datavec.api.util ClassPathResource]
           [org.deeplearning4j.models.word2vec Word2Vec$Builder]
           [org.deeplearning4j.models.embeddings.loader WordVectorSerializer]
           [org.deeplearning4j.text.stopwords StopWords]))

(defn render-bad-conversation-lines
  "Given two story texts, splice them to produce nonsensical exchanges.
  Take one author lines of s1, leave; then one author lines of s2 and shuffle.

  Interleave two shuffled lines."
  ([s] (render-bad-conversation-lines (first s) (second s)))
  ([con1 con2]
   (let [lines1 (-> con1 db/extract-author-parts ffirst second)
         lines2 (-> con2 db/extract-author-parts second first second)]
     (interleave lines1 (shuffle lines2)))))

(defn render-bad-conversation
  "Given a good pair of conversations, make a bad conversation out of them"
  ([s] (render-bad-conversation (first s) (second s)))
  ([s1 s2]
   (let [bad-lines  (render-bad-conversation-lines s1 s2)]
     {:other_info {:lines bad-lines} })))

(defn n-bad-conversations
  [& [n]]
  (let [conversation-pairs (db/get-conversation-pairs) ]
    (cond
      (and n (> n (count conversation-pairs)))
      (throw (ex-info "Too many requested of `n-bad-conversations`"
                      {:n n :limit (count conversation-pairs)}) )

      n
      (for [n (range n)] (render-bad-conversation (nth conversation-pairs n)))

      :else
      (for [c conversation-pairs] (render-bad-conversation c)))))

(defn import-model "Import a model based upon Google's News Corpus or other .gz training model
  as per https://deeplearning4j.org/word2vec.html"
  ([] (import-model (-> env :word2vec :model-file-path)))
  ([model-path]
   (-> model-path
       File.
       WordVectorSerializer/readWord2VecModel)))
                                        

(defn words-nearest
  "Top `n` words nearest `plus-words` minus `minus-words`"
  ([model plus-words n]
   (words-nearest model plus-words [] n))
  ([model plus-words minus-words n]
   (.wordsNearest model plus-words minus-words n)))

(defn sanitize-and-split
  "sanitize the sentence"
  [s]
  (-> s (str/split #" ")
      (->> (map #(str/replace % #"[\d\W]" "")))))

(defn words-mean
  "Get the word-vector mean in `model` for `words`"
  [model words]
  (let [words (if (string? words) (sanitize-and-split words) words) ]
    ;; (println "\n>>> Getting words-mean on ")
    ;; (prn words)
    (try
      (.getWordVectorsMean model words)
      (catch Exception e
        (.getWordVectorsMean model ["the"]) ;; if we work on a collection without any meaningful words, we otherwise get an error
        ))))

(defn similarity
  [model w1 w2]
  (.similarity model w1 w2))

(defn sentence-similarity
  "The squared distance dbetween two sentences.

  If they are identity, return 1."
  [model sentence1 sentence2]
  (let [squared-distance (.squaredDistance (words-mean model sentence1) (words-mean model sentence2))]
    (if (<= 1 squared-distance)
      squared-distance
      (- 1 squared-distance))))

(defn conversation-similarities
  "Given `lines-a` and `lines-b`, return the sequence of similarities between them"
  ([model two-side-conversation]
   (let [[lines-a lines-b] (map (comp second first seq) (db/extract-author-parts two-side-conversation))]
     (conversation-similarities model lines-a lines-b)))
  ([model lines-a lines-b]
   (for [[a b] (partition 2 (interleave (map :text lines-a) (map :text lines-b)))]
     (sentence-similarity model a b))))

(defn average-coll
  [c]
  (/ (apply + c) (count c)))
