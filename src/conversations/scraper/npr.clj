(ns conversations.scraper.npr
  "Scraping from the NPR infinite-scrolling mechanism"
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [clojure.java.io :as io]
            [net.cgrand.enlive-html :as html]
            [pl.danieljanus.tagsoup :as ts]
            [clojure.string :as str]
            [conversations.db.core :as db])
  (:import [java.net URL]
           [java.io File]))

(defn extract-stories [archive-html]
  (let [raw-story-entries (-> archive-html first :content)
        story-entries (filter (fn [i] (instance? clojure.lang.PersistentStructMap i)) raw-story-entries)
        entries-info (for [story story-entries] (-> story :attrs (select-keys [:data-episode-date :data-episode-id])))]
    entries-info))

(defn get-story-authors
  "Extract all author lines from story text"
  [story]
  (->> (filter not-empty (str/split-lines story))
       (map #(re-find #"^[A-Z 0-9,]+:" %))
       (filter not-empty)
       (map #(first (str/split % #":")))
       (map #(str/split % #","))
       (map (fn [s] (if (second s) (update-in s [1] str/trim) s)))))

(defn last-name
  [name]
  (last (str/split name  #" ")))

(defn get-unique-authors
  [story]
  (->> story get-story-authors (map first) (map last-name) set))

(defn get-story-text
  "Get the transcript text of a story at a url"
  [story-url]
  (let [transcript (-> story-url URL. html/html-resource (html/select [:div.transcript.storytext :p]))
        smap (map #(-> % :content first) (drop-last 2 transcript))]
    (clojure.string/join "\n\n" smap)))

(defn remove-author
  "Strip the initial '<author>:' line from a line of text"
  [line]
  (str/trim (str/replace line #"^[A-Z (),-]+:" "")))

(defn get-stories-and-authors
  "Return a sequence of lines as {:author a text t}"
  [story]
  (let [AUTHOR (atom nil)]
    (into []
          (for [line (filter not-empty (str/split-lines story))]
            (do 
              (when-let [this-author (first (get-unique-authors line))]
                (reset! AUTHOR this-author))
              {:author @AUTHOR
               :text (remove-author line)})))))

(defn get-atc-urls
  "Provided an index of All Things Considered episodes, returns urls for individual stories"
  [index-page-url]
  (let [html-base   (-> index-page-url URL. html/html-resource)]
    (map #(get-in % [:attrs :href]) (html/select html-base [:.program-segment__title :a]))))

(defn get-episode-ids
  "Get the episode-ids from articles on an index page"
  [index-page-url]
  (let [html-base (-> index-page-url URL. html/html-resource)]
    (map #(get-in % [:attrs :data-episode-id]) (html/select html-base [:article.program-show]))))


(defn get-atc-transcript-urls ;; TODO episodes, which include several things, don't have transcripts. 
  "Deduce the transcript urls from a page"
  [index-page-url]
  (let [urls (get-atc-urls index-page-url)]
    (map #(-> (str/split % #"/")
              butlast
              last
              (->> (str "https://www.npr.org/templates/transcript/transcript.php?storyId="))) urls)))
;; https://www.npr.org/templates/transcript/transcript.php?storyId=588345038


(defn get-next-atc-update
  "Get the next update for the infinite-scroll"
  [index-page-url]
  ;; get the last article.program-show
  (let [html-base   (-> index-page-url URL. html/html-resource)
        {:keys [data-episode-id data-episode-date]} (-> (html/select html-base [:article.program-show])
                                                        last
                                                        :attrs
                                                        (select-keys [:data-episode-id :data-episode-date]))]
    (str "https://www.npr.org/programs/all-things-considered/archive?date=" data-episode-date "&eid=" data-episode-id)))

(defn make-dirpath
  "Ensure the path is a valid, slash-ending one "
  [path-string]
  (let [file (File. path-string)]
    (if (and (.exists file)
             (.isDirectory file))
      (str file (File/separatorChar))
      (throw (ex-info "Invalid file: non-existing or not a directory" {:path path-string})))))


(defn get-n-new-atc
  "Get the story text for `n` atc. Output to `output-path`"
  [n output-dir-path]
  (let [index-page-url (atom "https://www.npr.org/programs/all-things-considered/archive")
        output-dir-path (make-dirpath output-dir-path)]
    (repeatedly n (fn [] (doseq [url (get-atc-transcript-urls @index-page-url)]
                           (let [output-file (str output-dir-path (last (str/split url #"=")))]
                             (if (.exists (io/file output-file))
                               (println "Already exists: " output-file " -- Skipping")
                               (do 
                                 (println "trying to download " url " to " output-file)
                                 (let [story (get-story-text url)]
                                   (if (empty? story)
                                     (println "No story retrieved for " url)
                                     (spit output-file story)))))))
                    (reset! index-page-url (get-next-atc-update @index-page-url))))))


(defn get-n-new-atc-db
  "Get the story text for `n` atc"
  [n]
  (let [index-page-url (atom "https://www.npr.org/programs/all-things-considered/archive")]
    (repeatedly n (fn [] (doseq [url (get-atc-transcript-urls @index-page-url)]
                           (let [story-id  (last (str/split url #"="))]
                             (if (db/get-conversation-by-url url)
                               (println "Already exists: " story-id " -- Skipping")
                               (do 
                                 (println "trying to insert " url)
                                 (let [story (get-story-text url)]
                                   (if (empty? story)
                                     (println "No story retrieved for " url)
                                     (db/CREATE :conversations {:origin_url url
                                                                :full_text story
                                                                :other_info {:lines (get-stories-and-authors story)}})))))))
                    (reset! index-page-url (get-next-atc-update @index-page-url))))))

(defn get-all-atc-db
  "Get the story text for `n` atc"
  []
  (let [prev-index-page-url (atom nil)
        index-page-url (atom "https://www.npr.org/programs/all-things-considered/archive")] ;(def index-page-url (atom "https://www.npr.org/programs/all-things-considered/archive"))
    (while (not (or (= @prev-index-page-url @index-page-url)
                    (nil? @index-page-url)))
      (doseq [url (get-atc-transcript-urls @index-page-url)]
        (let [story-id  (last (str/split url #"="))]
          (if (db/get-conversation-by-url url)
            (println "Already exists: " story-id " -- Skipping")
            (do 
              (println "trying to insert " url)
              (let [story (get-story-text url)]
                (if (empty? story)
                  (println "No story retrieved for " url)
                  (db/CREATE :conversations {:origin_url url
                                             :full_text story
                                             :other_info {:lines (db/remove-empty-lines (get-stories-and-authors story))}})))))))
      (reset! prev-index-page-url @index-page-url)
      (reset! index-page-url (get-next-atc-update @index-page-url)))))

(defn clean-conversation-lines
  "Remove the tags from all lines in the db"
  []
  (doseq [c (db/READ :conversations)]
    (db/UPDATE :conversations (:id c)
               (update-in c [:other_info :lines] #(map (fn [line-map] (update line-map :text remove-author)) %)))))
