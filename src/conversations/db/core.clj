(ns conversations.db.core
  "Database functionality for the conversations"
  (:require   [mount.core :refer [defstate]]
              [clojure.java.jdbc :as jdbc]
              [conversations.config :refer [env]]
              [clojure.string :as str]
              [cheshire.core :refer [generate-string parse-string]]
              [honeysql.core :as sql]
              [honeysql.helpers :as h]
              [hikari-cp.core :as hik])
  (:import org.postgresql.util.PGobject
           java.sql.Array
           clojure.lang.IPersistentMap
           clojure.lang.IPersistentVector
           com.mchange.v2.c3p0.ComboPooledDataSource
           [java.sql
            BatchUpdateException
            Date
            Timestamp
            PreparedStatement]))

(defn string-to-int [s]
  (if (= java.lang.String (type s))
    (Integer/parseInt s)
    s))

(defn string-to-date [s]
  (java.sql.Date/valueOf s))

(defn to-date [^java.sql.Date sql-date]
  (-> sql-date (.getTime) (java.util.Date.)))

(extend-protocol jdbc/IResultSetReadColumn
  Date
  (result-set-read-column [v _ _] (to-date v))

  Timestamp
  (result-set-read-column [v _ _] (to-date v))

  Array
  (result-set-read-column [v _ _] (vec (.getArray v)))

  PGobject
  (result-set-read-column [pgobj _metadata _index]
    (let [type  (.getType pgobj)
          value (.getValue pgobj)]
      (case type
        "json" (parse-string value true)
        "jsonb" (parse-string value true)
        "citext" (str value)
        value))))

(extend-type java.util.Date
  jdbc/ISQLParameter
  (set-parameter [v ^PreparedStatement stmt ^long idx]
    (.setTimestamp stmt idx (Timestamp. (.getTime v)))))

(defn to-pg-json [value]
  (doto (PGobject.)
    (.setType "jsonb")
    (.setValue (generate-string value))))

(extend-type clojure.lang.IPersistentVector
  jdbc/ISQLParameter
  (set-parameter [v ^java.sql.PreparedStatement stmt ^long idx]
    (let [conn      (.getConnection stmt)
          meta      (.getParameterMetaData stmt)
          type-name (.getParameterTypeName meta idx)]
      (if-let [elem-type (when (= (first type-name) \_) (apply str (rest type-name)))]
        (.setObject stmt idx (.createArrayOf conn elem-type (to-array v)))
        (.setObject stmt idx (to-pg-json v))))))

(extend-protocol jdbc/ISQLValue
  IPersistentMap
  (sql-value [value] (to-pg-json value))
  IPersistentVector
  (sql-value [value] (to-pg-json value)))

(defstate ^:dynamic *db*
  :start {:datasource (hik/make-datasource (-> env :db))}
  :stop (hik/close-datasource (:datasource *db*)))

;; CRUD
(defn dbc! [table entrymap] (jdbc/insert! *db* table entrymap))
(defn dbr [s] (jdbc/query *db* s))
(defn dbu! [table-key valmap where-vec] (jdbc/update! *db* table-key valmap where-vec))
(defn dbd! [table-key s] (jdbc/delete! *db* table-key s))
(defn dbdo! [s] (jdbc/execute! *db* s))

(defn CREATE
  "Generic item creation"
  [table-keyword valmap]
  (first (dbc! table-keyword valmap)))

(defn READ
  "Get anything from table by id, or all without id"
  [table-keyword &[id select-field-keys]]
  (cond-> {:select (or select-field-keys [:*])
           :from [table-keyword]}
    (not (nil? id)) (assoc :where [:= :id id])
    1 sql/format
    1 dbr
    (not (nil? id)) first
    ;(= 1 (count select-field-keys)) (#((first select-field-keys) %))
    ))

(defn UPDATE
  "Update anything from table by id"
  [table-keyword id valmap]
  (dbu! table-keyword valmap ["id = ?" id])
  (READ table-keyword id))

(defn DELETE
  "Generic delete"
  [table-keyword id]
  (dbd! table-keyword ["id = ?" id]))

;;;;;;;;;;;;;;;;;;;;;;
;; DB use functions ;;
;;;;;;;;;;;;;;;;;;;;;;

(defn get-conversation-by-url
  [url]
  (-> {:select [:*]
       :from [:conversations]
       :where [:= :origin_url url]}
      sql/format dbr first))

(defn get-conversations [&[id]]
  (cond-> {:select [:*] :from [:conversations]}
    id (merge {:where [:= :id id]})
    1 sql/format
    1 dbr
    id first))

(defn filter-lines-qmark
  "Get the questionmark-ending lines of a transcript's coll of text-line maps"
  [text-lines]
  (let [re #"\?"
        tx (filter #(re-find re %))]
    (transduce tx conj text-lines)))

(defn get-transcript-text-lines
  "Get just the text of lines in transcript"
  [transcript]
  (map :text (get-in transcript [:other_info :lines])))

(defn conversation?
  "Return whether a given transcript is a conversation and not just a news piece.
  At the moment this just means it has lines ending in question marks."
  [transcript]
  (not-empty (-> transcript get-transcript-text-lines filter-lines-qmark)))

(defn filter-questionmark-conversations
  ([] (filter-questionmark-conversations  (get-conversations)))
  ([conversations] (filter conversation? conversations)))

(defn get-authors
  "Get the author vector from a conversation"
  [c & [force?]]
  (or (and (not force?) (get-in c [:other_info :authors]))
      (distinct (map :author (-> c :other_info :lines)))))

(defn conversation-count
  "Get the total number of conversations in the db"
  []
  (-> {:select [:%count.*]
       :from [:conversations]} sql/format dbr first :count))

(defn filter-two-authors
  "Get conversations with only two interlocutors"
  ([] (filter-two-authors (get-conversations)))
  ([conversations]
   (filter #(= 2 (count (get-authors %))) conversations )))

(defn filter-conversant-lines
  "Only lines by `author`, associating the lines number of each turn"
  [author lines]
  (let [i-lines (map-indexed vector lines)]
    (for [[i l]
          (filter #(= author (:author (second %))) i-lines)]
      (assoc l :number i))))

(defn extract-author-parts
  "Given `transcript` return
  [{:author1 [author-1-lines]
  {:author2 [author-2-lines]}
  ...}]"
  [transcript]
  (let [authors (get-authors transcript)
        lines (get-in transcript [:other_info :lines])]
    (into []
          (for [author authors] {(keyword author) (filter-conversant-lines author lines)}))))

(defn all-good-conversations
  "Get just conversations with two authors and questionmark conversations"
  []
  (-> (get-conversations) filter-two-authors filter-questionmark-conversations))

(defn get-conversation-pairs
  "Get the n pair of good conversations"
  ([] (partition 2 (all-good-conversations)))
  ([n]
   (take n (partition 2 (all-good-conversations)))))

(defn nth-conversation-pair
  [n]
  (nth (get-conversation-pairs)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DB formatting functions ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn get-story-authors
  "Extract all author lines from story text"
  [story]
  (->> (filter not-empty (str/split-lines story))
       (map #(re-find #"^[A-Z 0-9,]+:" %))
       (filter not-empty)
       (map #(first (str/split % #":")))
       (map #(str/split % #","))
       (map (fn [s] (if (second s) (update-in s [1] str/trim) s)))))

(defn last-name
  [name]
  (last (str/split name  #" ")))

(defn get-unique-authors
  [story]
  (->> story get-story-authors (map first) (map last-name) set))

(defn remove-author
  "Strip the initial '<author>:' line from a line of text"
  [line]
  (str/trim (str/replace line #"^[A-Z (),-]+:" "")))

(defn get-stories-and-authors
  "Return a sequence of lines as {:author a text t}"
  [story]
  (let [AUTHOR (atom nil)]
    (into []
          (for [line (filter not-empty (str/split-lines story))]
            (do 
              (when-let [this-author (first (get-unique-authors line))]
                (reset! AUTHOR this-author))
              {:author @AUTHOR
               :text (remove-author line)})))))

(defn insert-conversation-authors
  "Insert the set of authors into the database for each conversation"
  ([]
   (doseq [c (READ :conversations)]
     (insert-conversation-authors c)))
  ([conversation]
   (UPDATE :conversations (:id conversation)
           (assoc-in conversation [:other_info :authors] (get-authors conversation :force)))))

(defn clean-conversation-lines
  "Remove the tags from all lines in the db"
  []
  (doseq [c (READ :conversations)]
    (UPDATE :conversations (:id c)
               (update-in c [:other_info :lines] #(map (fn [line-map] (update line-map :text remove-author)) %)))))

(defn remove-empty-lines  
  [lines]
  (filter #((complement empty?) (:text %)) lines))

(defn remove-empty-lines-db!
  "Remove lines from the database that are empty strings"
  []
  (doseq [c (get-conversations)]
    (let [id (:id c)]
      (println "Updating " id)
      (UPDATE :conversations id (update-in c [:other_info :lines] remove-empty-lines)))))


