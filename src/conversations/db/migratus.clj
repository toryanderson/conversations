(ns conversations.db.migratus
  (:require
   [conversations.config :refer [env]]
   [conversations.db.core :as db]
   [migratus.core :as migratus]))

(defn adapt-hikari-to-migratus [config]
  {:store :database
   :migration-dir "sql/"
   :init-script "init.sql"
   :migration-table-name "migrations"
   :db {:classname "org.postgresql.Driver"
        :subprotocol (:adapter config)
        :subname (str "//"
                      (:server-name config)
                      ":"
                      (:port-number config)
                      "/"
                      (:database-name config))
        :user (:username config)
        :password (:password config)}})

(defn config [] (adapt-hikari-to-migratus (-> env :db)))

(defn init []
  (migratus/init (config)))

(defn reset [] (migratus/reset (config)))

(defn renew []
  (init)
  (reset))
;;(renew)
