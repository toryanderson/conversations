(ns user
  (:require [mount.core :as m]
            [conversations.core]))

(def start mount.core/start)

(start)
